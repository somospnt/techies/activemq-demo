package com.pnt.mensageexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MensageExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MensageExampleApplication.class, args);
	}

}
